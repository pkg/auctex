Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: admin/* doc/*
Copyright: 1985-2023, Free Software Foundation, Inc
License: GPL-3+

Files: Makefile.in
Copyright: 2003-2008, 2010, 2013-2015, 2018-2023, Free Software
License: GPL-3+

Files: RELEASE
Copyright: no-info-found
License: GPL-3

Files: debian/*
Copyright: 1997-2023, Davide G. M. Salvetti
License: GPL-3+

Files: debian/auctex.doc-base.eperl
Copyright: 1997-2023, Davide G. M. Salvetti
License: GPL-2

Files: debian/patches/0007-Set-timestamp-on-generated-preview-latex.el-file.patch
Copyright: preview-latex.el ;
License: GPL-3+

Files: debian/rules
Copyright: 1997-2023, Davide G. M. Salvetti.
License: GPL-3+

Files: doc/Makefile.in
 doc/preview-dtxdoc.pl
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: doc/auctex.texi
 doc/preview-latex.texi
Copyright: 1992-1995, 2001-2022
License: GFDL-1.3+

Files: doc/intro.texi
Copyright: 2008, 2017, 2018, Free Software Foundation, Inc.
License: FSFAP or GPL-3

Files: install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: HPND-sell-variant

Files: latex/Makefile.in
 latex/preview.dtx
Copyright: 2001-2008, 2010, 2014, 2015, 2017-2023, Free Software Foundation
License: GPL-3+

Files: style/MyriadPro.el
 style/XCharter.el
 style/afterpage.el
 style/algpseudocode.el
 style/amsthm.el
 style/arabxetex.el
 style/attachfile.el
 style/baskervaldx.el
 style/beamerarticle.el
 style/bicaption.el
 style/bidi.el
 style/bigdelim.el
 style/bigstrut.el
 style/booktabs.el
 style/breqn.el
 style/caption.el
 style/changelog.el
 style/changes.el
 style/cleveref.el
 style/color.el
 style/comment.el
 style/csquotes.el
 style/currvita.el
 style/enumitem.el
 style/exam.el
 style/expl3.el
 style/fbb.el
 style/fbox.el
 style/filecontents.el
 style/floatrow.el
 style/fontaxes.el
 style/fontspec.el
 style/footmisc.el
 style/fvextra.el
 style/geometry.el
 style/graphicx.el
 style/harvard.el
 style/hologo.el
 style/ifluatex.el
 style/longtable.el
 style/ltablex.el
 style/ltugboat.el
 style/ltx-base.el
 style/ltxdoc.el
 style/ltxtable.el
 style/marginnote.el
 style/mdframed.el
 style/menukeys.el
 style/metalogo.el
 style/minted.el
 style/multido.el
 style/multind.el
 style/multirow.el
 style/nameref.el
 style/natbib.el
 style/newfloat.el
 style/nomencl.el
 style/ocg-p.el
 style/overpic.el
 style/paracol.el
 style/pdfpages.el
 style/physics.el
 style/pythontex.el
 style/shortvrb.el
 style/splitidx.el
 style/subcaption.el
 style/tcolorbox.el
 style/tcolorboxlib-raster.el
 style/tex-live.el
 style/textpos.el
 style/thm-restate.el
 style/thmtools.el
 style/titleps.el
 style/titlesec.el
 style/titletoc.el
 style/varioref.el
 style/wrapfig.el
 style/xcolor.el
 style/xltabular.el
 style/xparse.el
 style/xr-hyper.el
 style/xspace.el
Copyright: 1994, 1997-2000, 2003-2005, 2007, 2009, 2011-2022
License: GPL-3+

Files: style/bidibeamer.el
 style/scrbook.el
 style/scrlttr2.el
 style/scrreprt.el
Copyright: 2001-2008, 2010, 2014, 2015, 2017-2023, Free Software Foundation
License: GPL-3+

Files: style/scrartcl.el
Copyright: 2002, 2005, 2020, Free Software Foundation
License: GPL

Files: images/prvwrk24.xbm
Copyright: 1985-2023, Free Software Foundation, Inc
License: GPL-3+

Files: doc/changes.texi doc/faq.texi doc/fdl.texi doc/install.texi doc/preview-faq.texi doc/todo.texi doc/wininstall.texi
Copyright: 1992-2023, Free Software Foundation, Inc
License: GFDL-NIV-1.3

Files: doc/tex-ref.tex
Copyright: 1987-2023, Free Software Foundation, Inc
License: preserve-notice
